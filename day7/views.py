# encoding: utf-8
from django.http import HttpResponse
from django.conf import settings
from datetime import datetime
from django.utils import dateformat
from django.shortcuts import render_to_response
from locale import setlocale, LC_ALL


def to_date(date):
    return datetime.strptime(date, '%d.%m.%Y')


def today(request):
    return HttpResponse(dateformat.format(datetime.today(), settings.DATE_FORMAT))


def date_by_weekday(request, weekday):
    return HttpResponse("date-by-weekday") #todo


def weekday_by_date(request, date):
    setlocale(LC_ALL, 'ru_RU.UTF-8')
    return HttpResponse(to_date(date).strftime("%A"))


def date_range(request, date_begin, date_end):
    return HttpResponse((to_date(date_end) - to_date(date_begin)).days)


def tax(request):
    salary = float(request.GET.get('salary'))
    context = {
        'salary': salary,
        'ndfl': salary * 0.13,
        'pf': salary * 0.22,
        'fss': salary * 0.029,
        'oms': salary * 0.051,
        'fssn': salary * 0.002,
        'total': salary * 0.302
    }
    return render_to_response('tax.html', context)
