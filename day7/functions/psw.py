#coding utf-8
import re


class CheckPsw(object):

    def __init__(self, psw):
        self.psw = str(psw)

    def is_digit(self):
        return bool(re.match(r'^[0-9]{4,}', self.psw))

    def is_latin(self):
        return bool(re.match(r'^[a-z]{6,10}$', self.psw))

    def is_middle_psw(self):
        return bool(re.match(r'^[a-zA-Z0-9@._-]{6,10}$', self.psw))

    def is_strong_psw(self):
        return bool(re.match(r'^[a-zA-Z0-9@._-]{6,10}$', self.psw) and
                    re.search(r'[a-z]+', self.psw) and
                    re.search(r'[A-Z]+', self.psw) and
                    re.search(r'[0-9]+', self.psw) and
                    re.search(r'[@._-]+', self.psw))


def phone_num(num):
    return ''.join(re.findall(r'\w[0-9]+', num))


def is_email(email):
    return bool(re.match(r'^[a-zA-Z0-9_-]+@[a-zA-Z]+.[a-zA-Z]+$', email))


class Fruits(object):

    def __init__(self, str_fruits):
        self.str_fruits = str_fruits

    def count_fruits(self):
        pass


print CheckPsw('1234').is_digit()
print CheckPsw('abcdefg').is_latin()
print CheckPsw('abc@D-ef_g').is_middle_psw()
print CheckPsw('abc0@Def_').is_strong_psw()
print phone_num('+7 (929) 997-9999')
print phone_num('8-999-456-79-99')
print is_email('abc@De.fg')

fruit = Fruits(u'Яна съела яблок: 3. Аня съела яблок: 4. Андрей съел дынь: 5. Максим съел яблок: 2.')