"""day7 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from datetime import datetime
from django.conf.urls import include, url
from django.contrib import admin
from views import today, date_by_weekday, weekday_by_date, date_range, tax

urlpatterns = (
    url(r'^date/today$', today),
    url(r'^date-by-weekday/day-of-week/(?P<weekday>[a-z])$', date_by_weekday),
    url(r'^weekday-by-date/(?P<date>\d{1,2}.\d{1,2}.\d{4})$', weekday_by_date),
    url(r'^date-range/(?P<date_begin>\d{1,2}.\d{1,2}.\d{4})&(?P<date_end>\d{1,2}.\d{1,2}.\d{4})$', date_range),
    url(r'^tax$', tax),
    url(r'^admin/', admin.site.urls),
)
